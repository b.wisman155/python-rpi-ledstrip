from flask import request, render_template

from main import app
from control import start_breathing

@app.route("/breathing", methods=["GET", "POST"])
def breathing():
    if request.method == "GET":
        return render_template("breathing.html")
    elif request.method == "POST":
        delay = request.form["delay"]
        start_breathing(int(delay))
        return "post"
    else:
        return "Method not allowed"

