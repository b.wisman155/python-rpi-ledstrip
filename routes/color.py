from flask import request, redirect, render_template
from base64 import b16encode

from control import fill_color
from main import app
from utility import text_to_rgb

current_color = (255, 0, 0)


@app.route('/color', methods=["GET", "POST"])
def color():
    global current_color
    if request.method == "GET":
        result = (b'#'+b16encode(bytes(current_color))).decode("utf-8")
        return render_template("color.html", color=result)
    elif request.method == "POST":
        rgb = text_to_rgb(request.form["color"])
        fill_color(rgb + (0,))
        current_color = rgb
        return redirect("/color")
    else:
        return "Method not allowed"
