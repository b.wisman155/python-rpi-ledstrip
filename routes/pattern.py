from flask import request, render_template, redirect

from main import app
from utility import text_to_rgb
from control import show_pattern

current_pattern = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]


@app.route('/pattern', methods=["GET", "POST"])
def pattern():
    global current_pattern
    if request.method == "GET":
        return render_template("pattern.html", pattern=current_pattern)
    elif request.method == "POST":
        p = [text_to_rgb(request.form["color1"]),
             text_to_rgb(request.form["color2"]),
             text_to_rgb(request.form["color3"])]
        current_pattern = p
        show_pattern(p)
        return redirect("/pattern")
    else:
        return "Method not allowed"
