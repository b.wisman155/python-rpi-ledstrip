import board
import neopixel
import threading
from time import sleep

pixel_count = 150
pixels = neopixel.NeoPixel(board.D18, pixel_count, pixel_order=neopixel.GRBW, auto_write=False, brightness=0.2)

thread_ids = []
stop_threads = []


def get_status():
    status = {
        "on": 1 in [(i[0] > 0 or i[1] > 0 or i[2] > 0) for i in pixels]
    }
    return status


def fill_color(colors):
    global pixels
    stop_all_threads()
    pixels.fill(colors)
    pixels.show()


def set_individual(index, colors):
    global pixels
    stop_all_threads()
    change_animated(index, colors)


def change_animated(index, color):
    change_thr = threading.Thread(target=animated_thread, args=(index, color))
    change_thr.start()


def animated_thread_all(target_colors):
    global pixels
    done = []

    while(len(done) != pixel_count):
        for i in range(0, pixel_count):
            color = target_colors[i]
            if  pixels[i][0] == color[0] and 
                pixels[i][1] == color[1] and 
                pixels[i][2] == color[2]:
                if (i + 1) not in done:
                    done.append(i + 1)

                continue
            p = pixels[i]
            
            c1 = p[0] + (1 if p[0] < color[0] else -1)
            c2 = p[1] + (1 if p[1] < color[1] else -1)
            c3 = p[2] + (1 if p[2] < color[2] else -1)
            
            new_color = (
                (c1 if p[0] != color[0] else p[0]),
                (c2 if p[1] != color[1] else p[1]),
                (c3 if p[2] != color[2] else p[2])
            )
            pixels[index] = new_color
            pixels.show()
            sleep(10 / 1000)
    


def animated_thread(index, color):
    global pixels
    while(pixels[index][0] != color[0] or 
          pixels[index][1] != color[1] or
          pixels[index][2] != color[2]
        ):
        p = pixels[index]
        
        c1 = p[0] + (1 if p[0] < color[0] else -1)
        c2 = p[1] + (1 if p[1] < color[1] else -1)
        c3 = p[2] + (1 if p[2] < color[2] else -1)
        
        new_color = (
            (c1 if p[0] != color[0] else p[0]),
            (c2 if p[1] != color[1] else p[1]),
            (c3 if p[2] != color[2] else p[2])
        )
        pixels[index] = new_color
        pixels.show()
        sleep(10 / 1000)



# pattern [colors, colors1, colors2, etc...] colors should be (0-255, 0-255, 0-255, 0-255)
def show_pattern(pattern): 
    global pixels
    stop_all_threads()
    p_index = 0
    for i in range(0, pixel_count):
        if p_index == len(pattern):
            p_index = 0
        set_individual(i, pattern[p_index])
        p_index += 1
    pixels.show()


def start_breathing(delay):
    global thread_ids
    stop_all_threads()
    tid = len(thread_ids)
    thread_ids.insert(0, tid)
    thd = threading.Thread(target=breathing, args=(delay, tid, ))
    thd.start()


def stop_all_threads():
    global thread_ids, stop_threads
    stop_threads += thread_ids
    thread_ids = []


def breathing(delay, tid):
    global stop_threads
    status = [0, 0, 255]
    index = 0
    while True:
        if status[index] == 255:
            if index == 2:
                index = 0
            else:
                index += 1

        if index == 0:
            status[2] -= 1
        else:
            status[index - 1] -= 1

        status[index] += 1

        pixels.fill(tuple(status))
        pixels.show()
        if tid in stop_threads:
            stop_threads.remove(tid)
            return
        sleep(delay / 1000)
