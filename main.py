from flask import Flask, request, render_template, redirect
from control import  pixels, get_status


app = Flask(__name__)


@app.route('/', methods=["GET"])
def home():
    return render_template("index.html", **get_status())


@app.route('/status', methods=["GET"])
def status():
    return str(pixels)


from routes import *
